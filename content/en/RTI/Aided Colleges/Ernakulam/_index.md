---
title: "Ernakulam Zone"
linkTitle: "Ernakulam Zone"
weight: 3
date: 2020-08-29
type: docs
description: >
  Accepted, Received 1 Reply
---

{{% pageinfo %}}
Accepted and Forwarded
{{% /pageinfo %}}

## Colleges

- [ ] Newmans College, Thodupuzha
- [ ] Pavanatma College, Murickasserry
- [ ] NSS College, Rajakumari
- [ ] St. Xaviers College, Vaikkom
- [ ] MES College, Marampally
- [ ] BPC College, Piravom
- [ ] SSV College, Valayan Chirangara
- [ ] St. Peters College, Kolenchery
- [ ] St. Josephs Training College for Women, Ernakulam
- [ ] NSS College, Cherthala
- [x] MA College, Kothamanagalam
- [ ] D.B. College, Thalayolaparambu
- [ ] St. Pauls College, Kalamassery
- [ ] Morning Star Home Science College, Angamaly
- [ ] Sanatana Dharma College, Alappuzha
- [ ] St. Alberts College, Ernakulam
- [ ] Sree Sankara College, Kalady.
- [ ] St. Therasas College, Ernakulam
- [ ] SNM Training College, Moothakunnam
- [ ] Aquinas College, Edacochi
- [ ] St. Xaviers College for Women, Aluva
- [ ] Rajagiri College of Social Science, Kalamassery
- [ ] Cochin College, Kochi
- [ ] Nirmala College, Muvattupuzha
- [ ] Al-Ameen College, Edathala, Aluva
- [ ] St. Josephs College for Women, Alappuzha
- [ ] Sacred Heart College, Thevara
- [ ] Bharatha matha College, Trikkakara
- [ ] Union Christian College, Aluva
- [ ] SN College, Cherthala
- [ ] St. Michaels College, Cherthala
- [ ] SNM College, Maliankarra
- [ ] Marthoma College for Women, Perumbavoor


