---
title: "Kottayam Zone"
linkTitle: "Kottayam Zone"
weight: 3
date: 2020-08-29
type: docs
description: >
  Accepted, Received 2 Replies
---

{{% pageinfo %}}
Accepted and Forwarded
{{% /pageinfo %}}

### Colleges

- [ ] CMS College, Kottayam
- [ ] NSS Training College, Changanasserry
- [ ] Assumption College Changanacherry
- [ ] St. Stephens College Uzhavoor
- [ ] Devamatha College Kuravilangad
- [ ] St. Dominic College Kanjirappally
- [ ] KE College Mannam
- [ ] Mount Carmel Training College, Kottayam
- [ ] St. Marys College Mannarcaud
- [ ] St. Berchamans College Changanassery
- [ ] Alphonsa College Pala
- [ ] St. Josephs Training College, Mannanam
- [ ] KG College Pampady
- [ ] St. George College Aruvithura
- [ ] BK College for Women Amalagiri
- [ ] St. Thomas College Pala
- [ ] Baselios College Kottayam
- [ ] Henry Baker College Melukavu
- [ ] NSS Hindu College Changanasserry
- [ ] St. Thomas Training College, Pala
- [ ] SVR NSS College Vazhoor
- [ ] BCM College Kottayam
- [ ] NSS College Pandalam
- [ ] DB Pampa College Parumala
- [ ] Catholicate College Pathanamthitta
- [ ] Titus II Teachers College, Thiruvalla
- [ ] Marthoma College, Thiruvalla
- [ ] BAM College Thuruthicaud
- [ ] St. Thomas College Kozhencherry
- [ ] SAS SNDP Yogam College Konni
- [ ] NSS Training College Pandalam
- [ ] St. Thomas College, Ranni
- [ ] Sri Ayyappa College Eramalikkara
- [ ] St. Aloysius College Chengannur
- [ ] Christian College Chengannur
- [ ] S.N. College Chengannur
- [ ] Marian College, Kuttikanam
- [ ] MES College Neduamkandam
- [ ] St. Josephs College Moolamattom
- [ ] SNDP College, Konni, Pathanamthitta
- [ ] Sree Narayana Arts & Science College Kmarakom
- [ ] S N College, Kumarakom




