---
title: "Kozhikkod Zone"
linkTitle: "Kozhikkod Zone"
weight: 5
date: 2020-08-29
type: docs
description: >
  Pending 
---

{{% pageinfo %}}
Awaiting Confirmation
{{% /pageinfo %}}

- [ ] Amal College of Advanced studies Nilampur,Malappuram
- [ ] Ansar Arabic College, Valavannur
- [ ] Anvarul Islam Arabic College, Kuniyl Arecode, Malappuram
- [ ] Anvarul Islam Women Arabic College, Mongam, Malappuram
- [ ] College-Operative Arts and Science College, Madayi
- [ ] Darul Uloom Arabic College, Vazhakkad, Malappuram
- [ ] Darul Irshad Arabic College, Thalassery
- [ ] Darunnajith Arabic college, Karuvarakandam
- [ ] EMEA College, Kondotty
- [ ] Farook College, Farook
- [ ] Farook Training College, Farook
- [ ] Keyi Sahib Training College, Taliparamba
- [ ] M.G.College, Iritty
- [ ] Madeerathul Uloom Arabic College, Pulikkal, Malappuram
- [ ] Malabar Christian College, Kozhikode.
- [ ] Mar Thoma college, Chungathara
- [ ] Mary Matha Arts and Science College,
- [ ] Nilampur,Malappuram, Mananthavady
- [ ] MEA Sullamussalam Science College, Arecode, Malappuram
- [ ] MES College, Ponnani
- [ ] MES Keeveeyam College, Valancherry
- [ ] MES Mampad College, Mampad
- [ ] Muhammed Adbu Rahiman Memorial Orphange College, Mannassery, Mukkam
- [ ] N.A.M.College, Kallikandy
- [ ] Nasarathul Islam Arabic College, Kadavathara, Malappuram
- [ ] Nehru Arts and Science College, Kanhangad
- [ ] Nirmalagiri college, Koothuparambu
- [ ] NSS College, Manjeri
- [ ] P.K.M.College of Education, Kannur
- [ ] Payyannur college, Payyannur
- [ ] Pazhassi raja NSS College, Mattannur
- [ ] Pazhassiraja College, Pulpally
- [ ] Providence Womens College, Kozhikode
- [ ] PSMO College, Thirurangadi
- [ ] R.Sankar Memorial S.N.D.P.Yogam Arts and Science College, Koilandy
- [ ] Rousathal Uloom Arabic College, Farook
- [ ] S.E.S.College, Sreekantapuram
- [ ] S.N.College, Thottada, Kannur
- [ ] S.N.Guru College, Chelannur
- [ ] Sir Syed College, Thaliparamba
- [ ] St.Joseph's college, Devagiri
- [ ] St.Marys College, Sulthanbattery
- [ ] St.Pius X College, Rajapuram
- [ ] Sullamussalam Arabic college, Areacode
- [ ] Sunnivva Arabic College, Chennamangallur, Unity womens College, Manjeri
- [ ] W.M.O.Arts and Science College, Muttil
- [ ] Zamorins Guruvayurappan College, Kozhikode



