---
title: "Kollam Zone"
linkTitle: "Kollam Zone"
weight: 2
date: 2020-08-29
type: docs
description: >
  Rejected
---

{{% pageinfo %}}
Rejected

Preparing first appeal
{{% /pageinfo %}}

### Colleges
- [ ] All Saints College, Thiruvananthapuram
- [ ] Christian College, Kattakada
- [ ] Iqbal College, Peringamala
- [ ] Loyolla College of Social Sciences, Thiruvananthapuram
- [ ] M.G.College, Thiruvananthapuram
- [ ] Mannania College of Arts and Science,Pangode, Thiruvananthapuram
- [ ] Mar Ivanios College,Thiruvananthapuram
- [ ] NSS College for Women,Thiruvananthapuram
- [ ] SN College Chempazhanthy, Thiruvananthapuram
- [ ] SN College, Varkala
- [ ] St. Xaviers College Thiruvananthapuram
- [ ] VTMNSS College Dhanuvachapuram
- [ ] DB College, Sasthamkotta
- [ ] Fathima Matha Naitonal College, Kollam
- [ ] MMNSS College, Kottiyam
- [ ] NSS College, Nilamel
- [ ] SN College, Chathannur
- [ ] SN College for Women, Kollam
- [ ] SN College, Kollam
- [ ] SN College, Punalur
- [ ] St. Gregorios College, Kottarakkara
- [ ] St. Johns College, Anchal
- [ ] St. Stephens College, Pathanapuram
- [ ] TKM Arts and Science College,Kollam
- [ ] Bishop Moor College, Mavelikkara
- [ ] Karmala Rani Training College, Kollam
- [ ] Peet Memorial Training College, Mavelikkara
- [ ] Mount Tabour Training College, Pathanapuram
- [ ] TKMM College, Nangiarkulangara
- [ ] SN Training College, Nedungada
- [ ] St Cyrils College, Adoor
- [ ] Mar Theophilus Training College, Thiruvananthapuram
- [ ] M.S.M College, Kayamkulam




