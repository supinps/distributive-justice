---
title: "Thrissur Zone"
weight: 4
date: 2020-08-29
type: docs
description: >
  Pending 
---

{{% pageinfo %}}
Awaiting confirmation
{{% /pageinfo %}}

### Colleges

- [ ] NSS College, Nenmara, Palakkad
- [ ] Mercy College, Palakkad
- [ ] Sree Vivekananda College, Kunnamkulam, Thrissur
- [ ] Carmel College, Mala
- [ ] St. Marys College, Thrissur
- [ ] Sree Kerala Varma College, Thrissur
- [ ] St. Aloysius College, Elthuruth
- [ ] NSS College, Ottappalam
- [ ] St. Thomas College, Thrissur
- [ ] Little Flower College, Guruvayoor
- [ ] Sacred Heart College, Chalakudy
- [ ] St. Josephs College, Irinjalakuda
- [ ] Prajyothi Niketan College, Pudukkad
- [ ] Sree Krishna College, Guruvayoor
- [ ] SN College Nattika, Thrissur
- [ ] Vimala College, Thrissur
- [ ] MES Kalladi College Mannarkkad, Palakkad
- [ ] Sree Vyasa NSS College, Vadakanchery
- [ ] SN Trust College Shornur, Palakkad
- [ ] VTB College Mannamputta, Palakkad
- [ ] Christ College, Irinjalakuda
- [ ] MES Asmabi College, P. Vemballur
- [ ] MD College, Pazhanji
- [ ] NSS Training College, Ottappalam
- [ ] S N College , Alathur



